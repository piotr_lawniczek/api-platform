<?php

namespace App\DataPersister;

use App\Manager\UserManager;
use App\DTO\UserData;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Repository\UserRepository;

/**
 * Class UserDataPersister
 * @package App\DataPersister
 */
class UserDataPersister implements DataPersisterInterface
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserDataPersister constructor.
     * @param UserManager $userManager
     * @param UserRepository $userRepository
     */
    public function __construct(UserManager $userManager, UserRepository $userRepository)
    {
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $data
     * @return bool
     */
    public function supports($data): bool
    {
        return $data instanceof UserData;
    }

    /**
     * @param UserData $data
     * @return UserData
     */
    public function persist($data): UserData
    {
        $user = $this->userManager->create($data->email, $data->password, $data->roles);
        if (false === $data->isActive) {
            $user = $this->userManager->deactivate($user);
        }
        return new UserData($user);
    }

    /**
     * @param $data
     */
    public function remove($data): void
    {
        $user = $this->userRepository->find($data->id);
        if (!$user) {
            return;
        }
        $this->userManager->delete($user);
    }
}
