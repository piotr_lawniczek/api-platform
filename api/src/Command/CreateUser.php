<?php

namespace App\Command;

use App\DTO\UserData;
use App\Manager\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\SymfonyQuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CreateUser
 * @package App\Command
 */
class CreateUser extends Command
{
    /**
     * @var SymfonyQuestionHelper
     */
    private $questionHelper;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * CreateUser constructor.
     * @param UserManager $userManager
     * @param ValidatorInterface $validator
     */
    public function __construct(UserManager $userManager, ValidatorInterface $validator)
    {
        parent::__construct(null);
        $this->userManager = $userManager;
        $this->validator = $validator;
    }


    /**
     *
     */
    protected function configure(): void
    {
        $this->configureInfo();
        $this->configureOptions();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->questionHelper = $this->getHelper('question');

        $output->writeln('Creating new user');

        $data = new UserData();

        $data->email = $this->askForEmail($input, $output);
        $data->password = $this->askForPassword($input, $output);
        $data->roles = $this->askForRoles($input, $output);

        $errors = $this->validator->validate($data, null, 'register');

        if (count($errors) > 0) {
            $output->writeln('<error>Validation errors:</error>');
            $output->writeln('<error>==================</error>');
            /** @var ConstraintViolationInterface $error */
            foreach ($errors as $error) {
                $errMessage = '<error>' . ucfirst($error->getPropertyPath()) . ': ' . $error->getMessage() . '</error>';
                $output->writeln($errMessage);
            }
            $output->writeln('<error>Please try again</error>');
            return;
        }

        try {
            $user = $this->userManager->create($data->email, $data->password, $data->roles);
        } catch (\InvalidArgumentException $e) {
            $output->writeln('<error>' . $e->getMessage() . ' ,please try again</error>');
            return;
        }
        if ($input->getOption('inactive')) {
            $user = $this->userManager->deactivate($user);
        }
        $output->writeln('===============================================================');
        $output->writeln('Created new user with UUID: ' . $user->getId()->toString());
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return string
     */
    private function askForEmail(InputInterface $input, OutputInterface $output): string
    {
        $emailQuestion = new Question('Email: ');
        $email = $this->questionHelper->ask($input, $output, $emailQuestion);
        return $email;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return string
     */
    private function askForPassword(InputInterface $input, OutputInterface $output): string
    {
        $passwordQuestion = new Question('Password: ');
        $password = $this->questionHelper->ask($input, $output, $passwordQuestion);
        return $password;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return array
     */
    private function askForRoles(InputInterface $input, OutputInterface $output): array
    {
        $availableRoles = [
            'ROLE_USER',
            'ROLE_ADMIN'
        ];
        $message = 'Roles (Select by giving comma separated indexes eg. "0,1". Default ROLE_USER): ';
        $roleQuestion = new ChoiceQuestion($message, $availableRoles, 0);
        $roleQuestion->setMultiselect(true);
        $roleQuestion->setAutocompleterValues($availableRoles);

        $roles = $this->questionHelper->ask($input, $output, $roleQuestion);
        return $roles;
    }

    /**
     *
     */
    private function configureInfo(): void
    {
        $this
            ->setName('app:create-user')
            ->setDescription('Creates new user')
            ->setHelp('This command allows you to create new user');
    }

    /**
     *
     */
    private function configureOptions(): void
    {
        $this->addOption('inactive', null, InputOption::VALUE_NONE, 'Should user be inactive?');
    }
}
