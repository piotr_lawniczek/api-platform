<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\DTO\UserData;
use App\Entity\User;
use App\Repository\UserRepository;

/**
 * Class UserCollectionDataProvider
 * @package App\DataProvider
 */
class UserCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserItemDataProvider constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $resourceClass
     * @param string|null $operationName
     * @param array $context
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return UserData::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param string|null $operationName
     * @return array
     */
    public function getCollection(string $resourceClass, string $operationName = null): array
    {
        return array_map([$this, 'mapUserToDTO'], $this->userRepository->findAll());
    }

    /**
     * @param User $user
     * @return UserData
     */
    private function mapUserToDTO(User $user): UserData
    {
        return new UserData($user);
    }
}
