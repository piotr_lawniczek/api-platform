<?php

namespace App\DataProvider;

use App\DTO\UserData;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Repository\UserRepository;

/**
 * Class UserItemDataProvider
 */
class UserItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserItemDataProvider constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $resourceClass
     * @param string|null $operationName
     * @param array $context
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return UserData::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param array|int|string $id
     * @param string|null $operationName
     * @param array $context
     * @return UserData|null
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?UserData
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return null;
        }
        return new UserData($user);
    }
}
