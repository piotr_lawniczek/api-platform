<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceOne;
use FOS\OAuthServerBundle\Document\AccessToken;
use FOS\OAuthServerBundle\Model\ClientInterface;
use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class OAuthAccessToken
 * @Document(collection="oauthAccessToken")
 */
class OAuthAccessToken extends AccessToken
{
    /**
     * @var string
     * @Id()
     */
    protected $id;

    /**
     * @var OAuthClient
     * @Field(name="client")
     * @ReferenceOne(targetDocument="OAuthClient")
     */
    protected $client;

    /**
     * @var string
     *
     * @Field(name="userId", type="string")
     */
    protected $userId;

    /**
     * @param ClientInterface $client
     * @return OAuthAccessToken
     */
    public function setClient(ClientInterface $client): self
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(UserInterface $user): self
    {
        $this->userId = $user->getId()->toString();
        $this->user = $user;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
}
