<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use FOS\OAuthServerBundle\Document\Client;

/**
 * Class OAuthClient
 * @Document(collection="oauthClient")
 */
class OAuthClient extends Client
{
    /**
     * @var integer
     * @Id()
     */
    protected $id;
}
