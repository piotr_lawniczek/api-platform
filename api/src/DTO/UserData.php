<?php

namespace App\DTO;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\User;
use ApiPlatform\Core\Annotation\ApiResource;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\Model
 * @ApiResource(
 *     shortName="user",
 *     collectionOperations={
 *          "get"={"normalization_context"={"groups"={"read"}}},
 *          "post"={
 *              "validation_groups"={"register"},
 *              "normalization_context"={"groups"={"read"}},
 *              "denormalization_context"={"groups"={"write"}}
 *          }
 *     },
 *     itemOperations={
 *          "delete"={"normalization_context"={"groups"={"read"}}},
 *          "get"={"normalization_context"={"groups"={"read"}}},
 *          "put"={"normalization_context"={"groups"={"read"}}}
 *     }
 * )
 */
class UserData
{
    /**
     * @var UuidInterface
     *
     * @ApiProperty(identifier=true)
     *
     * @Groups({"read"})
     */
    public $id;

    /**
     * @var string
     *
     * @Assert\Email(groups={"register"})
     *
     * @Groups({"read", "write"})
     */
    public $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"register"})
     *
     * @Groups({"write"})
     */
    public $password;

    /**
     * @var array
     *
     * @Groups({"write"})
     */
    public $roles;

    /**
     * @var boolean
     *
     * @Groups({"write"})
     */
    public $isActive;

    /**
     * UserData constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        if ($user) {
            $this->from($user);
        }
    }

    /**
     * @param User $user
     * @return UserData
     */
    public function from(User $user): self
    {
        $this->id = $user->getId();
        $this->email = $user->getEmail();
        $this->password = $user->getPassword();
        $this->roles = $user->getRoles();
        $this->isActive = $user->isActive();
        return $this;
    }
}
