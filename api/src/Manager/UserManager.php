<?php

namespace App\Manager;

use App\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserManager
 */
class UserManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param string $email
     * @param string $password
     * @param array $roles
     * @return User
     */
    public function create(string $email, string $password, array $roles = null): User
    {
        $user = new User();
        $encodedPassword = $this->passwordEncoder->encodePassword($user, $password);
        try {
            $user
                ->setEmail($email)
                ->setPassword($encodedPassword)
                ->setRoles($roles ?? ['ROLE_USER']);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new \InvalidArgumentException('User with given email already exists');
        }

        return $user;
    }

    /**
     * @param User $user
     */
    public function delete(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    /**
     * @param User $user
     * @return User
     */
    public function deactivate(User $user): User
    {
        $user->setIsActive(false);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function activate(User $user): User
    {
        $user->setIsActive(true);
        $this->entityManager->flush();

        return $user;
    }
}
