<?php

namespace App\DBAL;

use App\Repository\UserRepository;
use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;

class DocumentWithUserPreLoadSubscriber implements EventSubscriber
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * DocumentWithUserPreLoadSubscriber constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getSubscribedEvents()
    {
        return [Events::postLoad];
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        if (method_exists($args->getObject(), 'getUserId') && $args->getObject()->getUserId()) {
            $docWithUser = $args->getDocument();
            $user = $this->userRepository->find($docWithUser->getUserId());
            $docWithUser->setUser($user);
        }
    }
}
